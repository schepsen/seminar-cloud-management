## PROJECT ##

* ID: **S**eminar!**CM** (Cloud Management)
* Contact: schepsen@web.de, nicole.bialas@web.de

## THESE ##

Service-Level-Agreement (SLA)

## USAGE ##

Compile the latex-sources or use the latest released builds

## CHANGELOG ##

### Seminar!CM 0.2, updated @ 2017-05-30 ###

* added Kapitel 3 (Realisierung von SLA)


### Seminar!CM 0.1, updated @ 2017-05-08 ###

* added Themenvorstellung (Folien)
