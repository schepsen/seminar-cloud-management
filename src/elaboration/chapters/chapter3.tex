\chapter{Realisierung von SLA}

Im vorherigen Kapitel wurden die wichtigsten Begriffe aus dem SLA-Bereich eingeführt sowie dessen von Ron, S. et al. \cite{A:RSC} und Sun Microsystems Internet Data Center Group \cite{A:SSC} definierte wesentliche Lebensabschnitte erläutert. Die während dieser Zeit zu Hilfe genommenen Werkzeuge in Form von Dokumentationserstellungstools, Beschreibungssprachen, die wohlgeformt und präzis vereinbarbare Leistungsanforderungen an Dienste, Verantwortlichkeiten sämtlicher Vertragspartner sowie Vorgehensweisen und Haftungsfragen bei Nichteinhaltung irgendeines Service-Levels erfassen, und Frameworks wurden entweder kurz oder abstrakt in Bezug auf einen bestimmten Lebenszyklus der SLAs erwähnt, ohne die technische Seite der dahinterstehenden Technologie näher zu beleuchten. In diesem Kapitel wird dementsprechend versucht, auf diesen Punkt einzugehen, indem eine der weit verbreiteten SLA-Beschreibungssprachen\footnote{Zum Demonstrationszweck wird das Web Services Agreement (WS-Agreement) genommen.} sowie ein von IBM entwickeltes Framework namens Web Service Level Agreement (WSLA) präsentiert wird. Ein kleiner Überblick über weitere existierende Mittel werden der Vollständigkeit halber aufgefasst und tabellarisch im Anhang zu dieser Arbeit aufgeführt.

\section{WS-Agreement}

Eine dieser Sprachen ist ein Teil des durch das Open Grid Forum (OGF) entwickelten Standards \cite{M:WSA} zur Erstellung von SLAs, der zusätzlich noch ein Protokoll zur Kommunikation zwischen allen beteiligten Parteien (siehe Abb. \ref{ABB:WS-A-PRO}), Überwachung und Verwaltung von Agreements mittels einer WSDL\footnote{Die Web Services Description Language ist eine Beschreibungssprache für Netzwerkdienste.} basierten Schnittstelle zur Verfügung stellt.

\subsection{Allgemeines über die Sprache}

Web Services Agreement ist eine Extensible Markup Language (XML) basierte Sprache, die das Festlegen, Verhandeln und Verwalten von Vereinbarungen selbst zur Laufzeit zwischen Agreement Initiatoren und Agreement Respondern ermöglicht. Dank breiter Verbreitung des Standards lässt sie sich laut der Spezifikation bereits schon bei dem 1. Zyklusabschnitt, dem sogenannten Discovery-Schritt, einsetzen, indem Service Level Agreements in Form von Vorlagen angefertigt und bereitgestellt werden. Die auf diesem Wege erzeugten Artefakte, sprich Templates, sind nicht nur für Menschen verständlich, sondern auch für Maschinen lesbar, was gegenseitiges Austauschen erleichtert und in Verbindung mit einem weiteren durch das OGF entwickelten Standard namens WS-Agreement Negotiation die Sprache um dynamisches Vereinbaren von SLAs erweitert. In Abb. \ref{ABB:WS-A-PRO} ist der oben erwähnte Kommunikationsprozess zwischen allen beteiligten Parteien gemäß WS-Agreement Protokoll ausführlicher schematisiert. Neben der ersten Phase, Discovery, sind auch die Anderen darin zu erkennen. So findet sich die zweite Phase wieder, indem der Agreement Initiator die bereitgestellten Templates nach eigenem Bedarf anpasst. Darauf folgend wird eine Offerte an den Agreement Responder versandt, der dann entscheidet, ob sie seinerseits erfüllbar ist und ob sie ihm generell passt. Im Falle eines positiven Verdiktes wird ein Agreement erstellt und dem Initiator ein Entry Point Reference (ERP) darauf mitgeteilt, über es er den Status des Service-Levels jederzeit abfragen kann. Anderenfalls kommt es zu einer Ablehnung der Offerte.

\begin{figure}[htbp]
	\centering
	\includesvg{pictures/ws-a-protokoll}	
	\caption{Die schematische Darstellung des WS-Agreement-Protokolls}
	\label{ABB:WS-A-PRO}
\end{figure}

Des Weiteren kann die Sprache dank flexibles Aufbaus um domänenspezifische Elemente erweitert und somit auch außerhalb der Web Service Domäne eingesetzt werden.

\subsection{Aufbau eines Agreements}

Jede mit WS-Agreement definierte Vereinbarung ist eine nach dem XML-Schema strukturierte Datei, die aus einfachen und komplexen Tags sowie Attributen besteht. Ein solches im Wurzelelement hinterlegtes Attribut namens \texttt{AgreementId} identifiziert jede Vereinbarung eindeutig. Darüber hinaus kann die Wurzel drei weitere Elemente \texttt{<wsag:Name>}, \texttt{<wsag:Context>} und \texttt{<wsag:Terms>} (siehe  Listing \ref{LST:WSASTRUCTURE}) enthalten. 

\begin{lstlisting}[caption = Der Aufbau einer mit WS-Agreement beschriebenen Vereinbarung, language = ws-agreement, label = LST:WSASTRUCTURE, captionpos = b]
<wsag:Agreement AgreementId = "xs:string">
	<wsag:Name>
		<!-- xs:string -->
	</wsag:Name> ?
	<wsag:AgreementContext>
		<!-- wsag:AgreementContextType -->
	</wsag:AgreementContext>
	<wsag:Terms>
		<!-- wsag:TermCompositorType -->
	</wsag:Terms>
</wsag:Agreement>
\end{lstlisting}

Mit dem ersten Tag lässt sich das Agreement etwas ausführlicher beschreiben. Allerdings ist es optional. Das Element \texttt{<wsag:Context>} listet seinerseits vereinbarungsspezifische Metadaten auf, wie z.B. den Agreement Initiator, den Agreement Responder, das Ablaufdatum der Vereinbarung und das zur Erstellung des Agreements verwendete Template (siehe Listing \ref{LST:WSAKONTEXT}).

\begin{lstlisting}[caption = Der mit WS-Agreement beschriebene Kontext einer Vereinbarung, language = ws-agreement, label = LST:WSAKONTEXT, captionpos = b]
<wsag:Context>
	<wsag:AgreementInitiator>
		<!-- xs:anyType -->
	</wsag:AgreementInitiator> ?
	<wsag:AgreementResponder>
		<!-- xs:anyType -->
	</wsag:AgreementResponder> ?	
	<wsag:ServiceProvider>
		<!-- wsag:RoleType -->
	</wsag:ServiceProvider>
	<wsag:ExpirationTime> <!-- xs:DateTime --> </wsag:ExpirationTime> ?
	<wsag:TemplateId> <!-- xs:string --> </wsag:TemplateId> ?
	<wsag:TemplateName> <!-- xs:string --> </wsag:TemplateName> ?
	<xs:any/> *
</wsag:Context>
\end{lstlisting}

Die vereinbarten Dienstleistungen, Verantwortlichkeiten sowie Haftungsangelegenheiten werden innerhalb des letzten Elementes \texttt{<wsag:Terms>} abgespeichert, wobei es dafür zwei Unterelemente \texttt{<wsag:ServiceDescriptionTerm>} und \texttt{<wsag:GuaranteeTerm>} (siehe Listing \ref{LST:WSAGARANTEETERM}) gibt. Im Ersten, so wie der Name schon sagt, werden Dienstbeschreibungen einzeln aufgelistet. Garantiebedingungen sowie verantwortungstragende Personen, die auch im Falle eines Ausfalls zu kontaktieren sind, werden in dem verbliebenen Element notiert. Dafür sind die Attribute \texttt{Name} und \texttt{Obligated} sowie die Tags \texttt{<wsag:ServiceScope>}, \texttt{<wsag:>QualifyingCondition} und \texttt{wsag:BusinessValueList>} vorgesehen.

\begin{lstlisting}[caption = Die mit WS-Agreement beschriebenen Bedingungen einer Vereinbarung, language = ws-agreement, label = LST:WSAGARANTEETERM, captionpos = b]
<wsag:GuaranteeTerm Name = "xs:string" Obligated = "wsag:ServiceRoleType">
	<wsag:ServiceScope ServiceName = "xs:string">
		<!-- xs:anyType -->
	</wsag:ServiceScope>
	<wsag:QualifyingCondition>
		<!-- xs:anyType -->
	</wsag:QualifyingCondition>
	<wsag:ServiceLevelObjective>
		<!-- xs:anyType -->
	</wsag:ServiceLevelObjective>
	<wsag:BusinessValueList>
		<!-- xs:anyType -->
	</wsag:BusinessValueList>
</wsag:GuaranteeTerm>
\end{lstlisting}

Ein ausführliches Beispiel, welches das Zusammenspiel aller zuvor vorgestellten Elemente einer mit WS-Agreement beschriebenen Vereinbarung zeigen soll, ist aus Platzgründen nur im Anhang \ref{APP:B} zu finden. %Nichtsdestotrotz wird die hier angeführten Code Snippets ein gutes Verständnis über die Struktur der Sprache und ihren möglichen Einsatz.

%\section{Zusammenfassung}