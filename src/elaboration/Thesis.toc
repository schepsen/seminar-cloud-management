\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Einf\IeC {\"u}hrung ins Thema}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Theoretische Grundlagen}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Realisierung von SLA}{3}{chapter.3}
\contentsline {section}{\numberline {3.1}WS-Agreement}{3}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Allgemeines \IeC {\"u}ber die Sprache}{3}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Aufbau eines Agreements}{4}{subsection.3.1.2}
\contentsline {chapter}{\numberline {4}Use Cases in UC-Systemen}{7}{chapter.4}
\contentsline {chapter}{\numberline {5}Herausforderungen und Fazit}{8}{chapter.5}
\contentsline {section}{\numberline {5.1}Herausforderungen}{8}{section.5.1}
\contentsline {section}{\numberline {5.2}Fazit}{8}{section.5.2}
\contentsline {chapter}{Anhang}{9}{section*.8}
\contentsline {chapter}{\numberline {A}SLA Frameworks und Sprachen}{10}{Anhang.a.A}
\contentsline {section}{\numberline {A.1}\IeC {\"U}berblick}{10}{section.a.A.1}
\contentsline {chapter}{\numberline {B}WS-Agreement (Beispiel)}{11}{Anhang.a.B}
