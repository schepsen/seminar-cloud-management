% --- [ LaTeX Document Class ] ----------- ] Introduction [ --- %

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Thesis}[2015/11/22 v0.9 LaTeX document class for a seminar elaboration]

\LoadClass[a4paper, 12pt, oneside]{book}

\RequirePackage{Thesis}

%\setlength{\oddsidemargin}{0.25in}	% 1.25in left margin 
%\setlength{\evensidemargin}{0.25in}	% 1.25in left margin (even pages)
%\setlength{\topmargin}{0.0in}		% 1in top margin
%\setlength{\textwidth}{6.0in}		% 6.0in text - 1.25in rt margin
%\setlength{\textheight}{9in}		% Body ht for 1in margins

% --- [ LaTeX Document Class ] ------------ ] Definitions [ --- %

\newcommand{\B}{\si{\byte}} % B
\newcommand{\KB}{\si{\kilo\byte}} % KB
\newcommand{\MB}{\si{\mega\byte}} % MB
\newcommand{\GB}{\si{\giga\byte}} % GB
\newcommand{\TB}{\si{\tera\byte}} % TB

\newcommand{\mq}{\texttt{\dq}}

% --- [ LaTeX Document Class ] ---- ] Methods & Functions [ --- %

% #1 Universität
% #2 Author
% #3 Matrikelnummer
% #4 Datum
% #5 Art der Arbeit
% #6 Titel
% #7 Betreuer
% #8 Lehrstuhl

\newcommand{\setGlobals}[8]
{
	\def\@university{#1}
	\author{#2}
	\def\@matriculationnumber{#3}
	\date{#4}
	\def\@thesistype{#5}
	\title{#6}
	\def\@career{#7}
	\def\@professorialchair{#8}
}

\addto\captionsngerman
{
	\let\appendixtocname\appendixname
	\let\appendixpagename\appendixname
}

% --- [ LaTeX Document Class ] -------------- ] Deckblatt [ --- %

\newcommand{\createCover}
{%
	\hypersetup
	{%
		pdftitle 	=	{\@title},%
		pdfsubject	=	{\@thesistype},%
		pdfauthor	=	{\@author},%
		pdfcreator	=	{\LaTeX\ with package \flqq hyperref\frqq}%
	}

	\extraBookmark{Titelblatt}{titleanc}%

		\begin{titlepage}			
		    \begin{center}
		    	{\LARGE \@university}\\[0.2cm]		
		    	{\@professorialchair}
		    	\vfill	
		    	\includegraphics[height=2.5cm]{Goethe}
		    	\vfill			
				{\Large \@thesistype}\\[0.5cm]								
				{\huge \textbf{\@title}}\\[2.0cm]		
				eingereicht von\\[0.5cm]
				{\@author}\\[3cm]	
				\vfill
				{\large \@date}\\[0.5cm]
		    \end{center}
		\end{titlepage}
		
		\thispagestyle{empty}
}

% --- [ LaTeX Document Class ] ------- ] Eides. Erklärung [ --- %

\newcommand{\createDeclaration}
{
	\section*{Eidesstattliche Erklärung}
	\sloppy
	Wir erklären hiermit an Eides statt, dass die hier vorliegende Arbeit ausschliesslich von uns, selbstständig und ohne Benutzung anderer als der angegebenen Hilfsmittel angefertigt wurde. Die aus fremden Quellen direkt oder indirekt übernommenen Gedanken sind als solche kenntlich gemacht.
	
	Die Arbeit wurde bisher in gleicher oder ähnlicher Form keiner anderen Prüfungs-kommission vorgelegt und auch nicht veröffentlicht.	
	\vfill
	\noindent Frankfurt am Main, den \today \hfill \rule{7.5cm}{.4pt}\par \hfill \noindent (\@author) \hspace{0.0cm}
}

% --- [ LaTeX Document Class ] ------------- ] Danksagung [ --- %

\newcommand{\createAcknowledgement}
{	
	\newpage
	\setcounter{page}{3}
	\section*{Danksagung}
	
	Hier kommt ein Text rein!
		
	\newpage
	\newpage
	\phantom{}	
}

% --- [ LaTeX Document Class ] ---------- ] PDF Bookmarks [ --- %

\newcommand{\extraBookmark}[2]{\pdfbookmark[1]{#1}{#2}}

% --- [ LaTeX Document Class ] -------------------- ] EOF [ --- %